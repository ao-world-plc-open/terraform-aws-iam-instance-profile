resource "aws_iam_role_policy" "sts_assumerole" {
  name  = "sts_assumerole"
  count = var.sts_assumerole
  role  = aws_iam_role.default_role.id

  lifecycle {
    create_before_destroy = true
  }

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
