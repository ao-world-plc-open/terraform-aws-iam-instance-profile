# Terraform Module: AWS IAM Instance Profile

Creating IAM instance profile.

* [Example Usage](#example-usage)
  * [Basic](#basic)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

```hcl
module "iam_profile_jenkins" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-iam-instance-profile.git?ref=v1.1.0"

  autoscaling_describe           = "0"
  autoscaling_suspend_resume     = "0"
  autoscaling_terminate_instance = "0"
  autoscaling_update             = "0"
  cw_readonly                    = "1"
  ec2_attach                     = "1"
  ec2_describe                   = "1"
  ec2_ebs_attach                 = "0"
  ec2_eni_attach                 = "0"
  ec2_write_tags                 = "0"
  elasticache_readonly           = "0"
  es_allowall                    = "0"
  firehose_streams               = "0"
  kinesis_streams                = "0"
  kms_decrypt                    = "1"
  kms_decrypt_arns               = aws_kms_key.puppet.arn
  kms_encrypt                    = "1"
  kms_encrypt_arns               = aws_kms_key.puppet.arn
  name                           = "${var.envtype}_iam_profile_jenkins"
  packer_access                  = "0"
  r53_update                     = "1"
  rds_readonly                   = "0"
  redshift_read                  = "0"
  s3_read_buckets                = ["my-bucket-1", "my-bucket-2"]
  s3_write_buckets               = ["tmc-nonprod-repo", "tmc-prod-repo"]
  sns_allowall                   = "0"
  sqs_allowall                   = "0"
  sts_assumerole                 = "0"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| name | The name prefix for the IAM role and instance profile | `string` | n/a | yes |
| autoscaling\_describe | Bit indicating whether to create a role policy to allow the Describe permission on Autoscaling Groups | `string` | `"0"` | no |
| autoscaling\_octopus\_mininum | Bit indicating whether to cerate a role policy that provides minimum permissions for Octopus deployments to interact with auto scaling | `string` | `"0"` | no |
| autoscaling\_suspend\_resume | Bit indicating whether to create a role policy to allow Suspend/Resume on Autoscaling Groups | `string` | `"0"` | no |
| autoscaling\_terminate\_instance | Bit indicating whether to create a role policy to allow termination of Autoscaled instances | `string` | `"0"` | no |
| autoscaling\_update | Bit indicating whether to create a role policy to allow the Update permission on Autoscaling Groups | `string` | `"0"` | no |
| aws\_policies | A list of AWS policies to attach, e.g. AmazonMachineLearningFullAccess | `list(string)` | `[]` | no |
| custom\_tags | A map of custom tags to apply to the IAM role | `map(string)` | `{}` | no |
| cw\_logs\_update | Bit indicating whether to create a role policy to allow log update permissions on a Cloudwatch service | `string` | `"0"` | no |
| cw\_readonly | Bit indicating whether to create a role policy to allow List/Get permissions on a Cloudwatch service | `string` | `"0"` | no |
| cw\_update | Bit indicating whether to create a role policy to allow Put permissions on a Cloudwatch service | `string` | `"0"` | no |
| ec2\_attach | Bit indicating whether to create a role policy to allow Attach* access to instances | `string` | `"0"` | no |
| ec2\_describe | Bit indicating whether to create a role policy for access to the ec2\_describe API | `string` | `"1"` | no |
| ec2\_ebs\_attach | Bit indicating whether to create a role policy to allow attaching Elastic Block Store volumes to instances, also grants DescribeVolume | `string` | `"0"` | no |
| ec2\_eni\_attach | Bit indicating whether to create a role policy to allow attaching Elastic Network Interfaces to instances, also grants Describe interfaces and Describe/Modify attributes | `string` | `"0"` | no |
| ec2\_write\_tags | Bit indicating whether to create a role policy to allow write of ec2 tags | `string` | `"0"` | no |
| elasticache\_readonly | Bit indicating whether to create a role policy to allow read permissions on an ElastiCache service | `string` | `"0"` | no |
| es\_allowall | Bit indicating whether to create a role policy to allow full access to Elasticsearch | `string` | `"0"` | no |
| firehose\_streams | Bit indicating whether to create a role policy to allow the PutRecordBatch permission to Firehose Streams | `string` | `"0"` | no |
| kinesis\_streams | Bit indicating whether to create a role policy to allow Get/Put/Describe access to Kinesis Streams | `string` | `"0"` | no |
| kms\_decrypt | Bit indicating whether to create a role policy to allow decryption using KMS | `string` | `"0"` | no |
| kms\_decrypt\_arns | Comma seperated list of KMS key ARNs that can be used for decryption | `string` | `""` | no |
| kms\_encrypt | Bit indicating whether to create a role policy to allow encryption using KMS | `string` | `"0"` | no |
| kms\_encrypt\_arns | Comma seperated list of KMS key ARNs that can be used for encryption | `string` | `""` | no |
| packer\_access | Bit indicating whether to create a role policy to allow access for Hashicorp Packer | `string` | `"0"` | no |
| r53\_update | Bit indicating whether to create a role policy to allow update of r53 zones | `string` | `"0"` | no |
| rds\_readonly | Bit indicating whether to create a role policy to allow read access to the a Relational Database Service | `string` | `"0"` | no |
| redshift\_read | Bit indicating whether to create a role policy to allow read access to Redshift, and assocated ec2/CloudWatch access | `string` | `"0"` | no |
| s3\_read\_buckets | A list of s3 buckets to create read role policies on | `list(string)` | `[]` | no |
| s3\_write\_buckets | A list of s3 buckets to create write role policies on | `list(string)` | `[]` | no |
| sns\_allowall | Bit indicating whether to create a role policy to allow full access to SNS | `string` | `"0"` | no |
| sqs\_allowall | Bit indicating whether to create a role policy to allow full access to SQS | `string` | `"0"` | no |
| ssm\_get\_params | Bit indicating whether to create a role policy to allow getting SSM parameters | `string` | `"0"` | no |
| ssm\_get\_params\_names | List of SSM parameter names to be allowed | `list(string)` | `[]` | no |
| ssm\_managed | Bit indicating whether to create a role policy to allow SSM management | `string` | `"0"` | no |
| ssmparameter\_allowall | Bit indicating whether to create a role policy to allow SSM parameter management | `string` | `"0"` | no |
| sts\_assumerole | Bit indicating whether to create a role policy to allow assume access to the Security Token Service | `string` | `"0"` | no |
| transcribe\_fullaccess | Bit indicating whether to create a role policy to allow full access to the Transcribe Service | `string` | `"0"` | no |

## Outputs

| Name | Description |
|------|-------------|
| profile\_arn | *string* The ARN of the instance profile |
| profile\_id | *string* The ID of the instance profile |
| profile\_name | *string* The name of the instance profile |
| profile\_path | *string* The path to the instance profile |
| profile\_role | *string* The IAM role associated with the instance profile |
| profile\_unique\_id | *string* The unique ID associated with the instance profile |
| role\_arn | *string* The ARN of the IAM role associated with the instance profile |
| role\_id | *string* The ID for the IAM role associated with the instance profile |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.