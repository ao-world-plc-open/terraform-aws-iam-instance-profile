resource "aws_iam_role_policy" "es_allowall" {
  name  = "es_allowall"
  count = var.es_allowall
  role  = aws_iam_role.default_role.id

  lifecycle {
    create_before_destroy = true
  }

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "es:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
