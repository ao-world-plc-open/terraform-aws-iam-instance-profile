# Change Log

## v1.1.0 (08/12/2021)

* Added .terraform-doc.yml to auto generate (mostly) README.md
* Added basic example config for README.md
* All variables and outputs have descriptions and types
* Ran `terraform fmt` on the repository
* Updated CONTRIBUTING.md to cover running fmt and ensuring docs are
  regenerated
* Added .gitlab-ci.yml to do basic checks for fmt and out of date docs

## v1.0.0 (28/05/2021)

* Added minimum provider version constraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled
* Ran terraform fmt on repo to start off standardised formatting

## v0.4.1 (04/11/2020)

* remaining changes for tf upgrade
* added final tf upgrade files

## v0.4.0 (03/11/2020)

* upgraded terraform to version v.0.13.4

## v0.3.0 (28/05/2020)

* Allows a custom map of tags to be applied to the IAM role
* Brings the SSM managed policy in line with AmazonSSMManagedInstanceCore

## v0.2.0

### Features

* Adds new features to the module
* Policy for EC2 instances to be managed via SSM
* S3 read only buckets must now be explicitly specified
* Autoscaling policy to allow instances to register/de-register themselves from target groups

### Fixes

* IAM:PassRole added to Packer Policy

## v0.1.0

### Features

* Initial Commit

### Fixes

*N/A*
