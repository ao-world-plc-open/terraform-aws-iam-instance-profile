module "iam_profile_jenkins" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-iam-instance-profile.git?ref=v1.1.0"

  autoscaling_describe           = "0"
  autoscaling_suspend_resume     = "0"
  autoscaling_terminate_instance = "0"
  autoscaling_update             = "0"
  cw_readonly                    = "1"
  ec2_attach                     = "1"
  ec2_describe                   = "1"
  ec2_ebs_attach                 = "0"
  ec2_eni_attach                 = "0"
  ec2_write_tags                 = "0"
  elasticache_readonly           = "0"
  es_allowall                    = "0"
  firehose_streams               = "0"
  kinesis_streams                = "0"
  kms_decrypt                    = "1"
  kms_decrypt_arns               = aws_kms_key.puppet.arn
  kms_encrypt                    = "1"
  kms_encrypt_arns               = aws_kms_key.puppet.arn
  name                           = "${var.envtype}_iam_profile_jenkins"
  packer_access                  = "0"
  r53_update                     = "1"
  rds_readonly                   = "0"
  redshift_read                  = "0"
  s3_read_buckets                = ["my-bucket-1", "my-bucket-2"]
  s3_write_buckets               = ["tmc-nonprod-repo", "tmc-prod-repo"]
  sns_allowall                   = "0"
  sqs_allowall                   = "0"
  sts_assumerole                 = "0"
}