output "profile_arn" {
  description = "*string* The ARN of the instance profile"
  value       = aws_iam_instance_profile.instance_profile.arn
}

output "profile_id" {
  description = "*string* The ID of the instance profile"
  value       = aws_iam_instance_profile.instance_profile.id
}

output "profile_name" {
  description = "*string* The name of the instance profile"
  value       = aws_iam_instance_profile.instance_profile.name
}

output "profile_path" {
  description = "*string* The path to the instance profile"
  value       = aws_iam_instance_profile.instance_profile.path
}

output "profile_role" {
  description = "*string* The IAM role associated with the instance profile"
  value       = aws_iam_instance_profile.instance_profile.role
}

output "profile_unique_id" {
  description = "*string* The unique ID associated with the instance profile"
  value       = aws_iam_instance_profile.instance_profile.unique_id
}

output "role_arn" {
  description = "*string* The ARN of the IAM role associated with the instance profile"
  value       = aws_iam_role.default_role.arn
}

output "role_id" {
  description = "*string* The ID for the IAM role associated with the instance profile"
  value       = aws_iam_role.default_role.id
}
